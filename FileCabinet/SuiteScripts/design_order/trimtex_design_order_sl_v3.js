/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search','./trimtex_design_order_cs_v2.js', 'N/task', 'N/file', 'N/render', 'N/runtime'],
/**
 * @param {record} record
 */
function(record, search, designorder, task, file, render, runtime) {
	
	var RECORD_TYPE = {'Assembly' : 'assemblyitem'}
	var JUNIOR_SIZE = ['1', '2', '3', '4', '5', '18', '19', '20', '21'];
	var INTERMEDIATE_MASTER_SIZE = ['18', '19', '20', '21'];
	var DESIGN_ORDER_REC_TYPE_ID = 'customrecord_sta_tx_do';
	var DESIGN_ORDER_ITEM_REC_TYPE_ID = 'customrecord_sta_tx_dodet';
	var DESIGN_ORDER_ITEM_DESIGN_REC_TYPE_ID = 'customrecord_sta_tx_itemdesign';
	var CREATE_DESIGN_ORDER = 'customdeploy_tx_do_create_design_order';
	var CREATE_DEFAULT_ITEM_DESIGN = 'customdeploy_tx_do_create_item_design';
	var CREATE_MATRIX_SUBITEM = 'customdeploy_tx_do_create_matrix_subitem';
	var SET_ITEM_TEAMSHOP = 'customdeploy_tx_do_set_item_teamshop';
	var SET_ITEM_DESIGN_TEAMSHOP = 'customdeploy_tx_do_set_design_teamshop';
	var RESET_ITEM_DESIGN_STATUS = 'customdeploy_tx_do_reset_design_status';
	var REFLECT_ITEM_STATUS_ON_DESIGN = 'customdeploy_tx_do_reflect_item_status';
	var LOCK_DESIGN_ORDER = 'customdeploy_tx_do_lock_design_order';
	var UPDATE_DESIGN_ORDER_ITEMS = 'customdeploy_tx_do_update_item';
	var DEFAULT_DESIGN_ORDER_STATUS = 1; //Started
	var DEFAULT_DESIGN_ORDER_ITEM_STATUS = 3; //Pending
    
	/**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	if(context.request.method == 'GET'){
    		switch (runtime.getCurrentScript().deploymentId){
    			case CREATE_DESIGN_ORDER:
    				context.response.write(createDesinOrder(context.request.parameters.offerId));
    				break;
    			case CREATE_DEFAULT_ITEM_DESIGN:
    				context.response.write(createDefaultItemDesign(context.request.parameters.designOrderId));
    				break;
    			case SET_ITEM_TEAMSHOP:
    				context.response.write(setDesignOrderItemTeamshop(context.request.parameters.designOrderId, context.request.parameters.teamshopId));
    				break;
    			case SET_ITEM_DESIGN_TEAMSHOP:
    				context.response.write(setDesignOrderItemDesignTeamshop(context.request.parameters.designOrderId, context.request.parameters.teamshopId));
    				break;
    			case REFLECT_ITEM_STATUS_ON_DESIGN:
    				context.response.write(JSON.stringify(reflectItemStatusOnDesignList(context.request.parameters.designOrderId)));
    				break;
    			case RESET_ITEM_DESIGN_STATUS:
    				context.response.write(JSON.stringify(resetItemDesignStatus(context.request.parameters.designOrderId)));
    				break;
    			case CREATE_MATRIX_SUBITEM:
    				context.response.write(JSON.stringify(createMatrixSubitem(context)));
    				break;
    			case LOCK_DESIGN_ORDER:
    				lockDesignOrder(context);
    				break;
    			case UPDATE_DESIGN_ORDER_ITEMS:
    				context.response.write(updateDesignOrderItems(context.request.parameters.offerId));
    				break;
    		}
    	}
    }
    
    var createDesinOrder = function(offerId){

    	var id = record.submitFields({
    		type : record.Type.ESTIMATE,
    		id : offerId,
    		values : {
    			custbody_sta_tx_do_create_status : 'PENDING'
    		}
    	});
    	
    	var createDesignOrderTask = task.create({
			taskType : task.TaskType.SCHEDULED_SCRIPT,
			scriptId : 'customscript_sta_tx_do_create_ss',
			params : {
				custscript_sta_tx_do_create_offer : offerId,
				custscript_sta_tx_do_action : 1
			}
		});
		
		
		var createDesignOrderTaskStatus = task.checkStatus(createDesignOrderTask.submit());
		
		return createDesignOrderTaskStatus.status;
    	
    };
    
    var createDefaultItemDesign = function(designOrderId){
    	return designorder.createItemDesign(designOrderId);
    }
    
    var createMatrixSubitem = function(context){
    	log.debug(context.request.parameters.teamshopId);
		var createMatrixTask = task.create({
			/*taskType : task.TaskType.MAP_REDUCE,
			scriptId : 'customscript_sta_tx_do_create_matrix_mr',
			params : {
				custscript_sta_tx_do_id_mr : context.request.parameters.designOrderId
				//custscript_sta_tx_do_matrix_data_mr : JSON.stringify(matrixItemsObj)
			}*/
		
			taskType : task.TaskType.SCHEDULED_SCRIPT,
			scriptId : 'customscript_sta_tx_do_create_ss',
			params : {
				custscript_sta_tx_do_id : context.request.parameters.designOrderId,
				custscript_sta_tx_do_teamshop_id : context.request.parameters.teamshopId,
				custscript_sta_tx_do_action : 3
			}
		
		});
		
		var createMatrixTaskStatus = task.checkStatus(createMatrixTask.submit())
		
		var id = record.submitFields({
    		type : DESIGN_ORDER_REC_TYPE_ID,
    		id : context.request.parameters.designOrderId,
    		values : {
    			custrecord_sta_dx_do_matrix_status : createMatrixTaskStatus.status
    		}
    	});
		
		//context.response.write(createMatrixTaskStatus.status);
		return createMatrixTaskStatus.status;
    };
    
    var setDesignOrderItemTeamshop = function(designOrderId, teamshopId){
		//return designorder.setItemTeamshop(designOrderId, teamshopId);
    	
    	var assignTeamshopTask = task.create({
			taskType : task.TaskType.SCHEDULED_SCRIPT,
			scriptId : 'customscript_sta_tx_do_create_ss',
			params : {
				custscript_sta_tx_do_id : designOrderId,
				custscript_sta_tx_do_teamshop_id : teamshopId,
				custscript_sta_tx_do_action : 2
			}
		});
		
		
		var assignTeamshopTaskStatus = task.checkStatus(assignTeamshopTask.submit());
		
		return assignTeamshopTaskStatus.status;
    	
    };
    
    var setDesignOrderItemDesignTeamshop = function(designOrderId, teamshopId){
    	return designorder.setDesignTeamshop(designOrderId, teamshopId);
    }
    
    var resetItemDesignStatus = function(designOrderId){
    	var doRec = record.load({ type : DESIGN_ORDER_REC_TYPE_ID, id : designOrderId });
		for(var i = 0; i < doRec.getLineCount('recmachcustrecord_sta_tx_dodet_parentlink'); i++){
			if(doRec.getSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_itemstatus', i) == 2)
			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_itemstatus', i, 1)
		}
		
		for(var i = 0; i < doRec.getLineCount('recmachcustrecord_sta_tx_itemdesign_do'); i++){
			doRec.setValue('custrecord_sta_tx_do_status', 5);
			if(doRec.getSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord_sta_tx_itemdesign_appr', i) != 3)
			doRec.setSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord_sta_tx_itemdesign_appr', i, 1)
		}
		
		return doRec.save();
    };
    
    var reflectItemStatusOnDesignList = function(designOrderId){
    	var doRec = record.load({ type : DESIGN_ORDER_REC_TYPE_ID, id : designOrderId, isDynamic : true });
		for (var i = 0; i < doRec.getLineCount('recmachcustrecord_sta_tx_itemdesign_do'); i++){
			doRec.selectLine('recmachcustrecord_sta_tx_itemdesign_do', i);
			var itemIndex = doRec.findSublistLineWithValue('recmachcustrecord_sta_tx_dodet_parentlink', 'id', 
					doRec.getCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_do','custrecord_sta_tx_itemdesign_parentlink'))
			
    		if(itemIndex > -1){
    			doRec.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord1400', 
    					doRec.getSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_itemstatus', itemIndex));
        		doRec.commitLine('recmachcustrecord_sta_tx_itemdesign_do');
    		}
		}
		return doRec.save();
    };
    
    var lockDesignOrder = function(context){
    	var errorPageFile = file.load({ id : './trimtex_design_order_error_page.html' });
		var renderer = render.create();
		
		renderer.templateContent = errorPageFile.getContents();
		renderer.renderToResponse({ response : context.response });
    };
    
    var updateDesignOrderItems = function(offerId){
    	
    	var createDesignOrderTask = task.create({
			taskType : task.TaskType.SCHEDULED_SCRIPT,
			scriptId : 'customscript_sta_tx_do_create_ss',
			params : {
				custscript_sta_tx_do_create_offer : offerId,
				custscript_sta_tx_do_action : 4
			}
		});
		
		
		var createDesignOrderTaskStatus = task.checkStatus(createDesignOrderTask.submit());
		
		return createDesignOrderTaskStatus.status;
    }
    
    return {
        onRequest: onRequest
    };
    
});
