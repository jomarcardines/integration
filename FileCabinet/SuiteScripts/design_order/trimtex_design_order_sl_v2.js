/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search','./trimtex_design_order_cs_v2.js', 'N/task', 'N/file', 'N/render', 'N/runtime'],
/**
 * @param {record} record
 */
function(record, search, designorder, task, file, render, runtime) {
	
	var RECORD_TYPE = {'Assembly' : 'assemblyitem'}
	var JUNIOR_SIZE = ['1', '2', '3', '4', '5'];
	var INTERMEDIATE_MASTER_SIZE = ['18', '19', '20', '21'];
	var DESIGN_ORDER_REC_TYPE_ID = 'customrecord_sta_tx_do';
	var DESIGN_ORDER_ITEM_REC_TYPE_ID = 'customrecord_sta_tx_dodet';
	var DESIGN_ORDER_ITEM_DESIGN_REC_TYPE_ID = 'customrecord_sta_tx_itemdesign';
	var CREATE_DESIGN_ORDER = 'customdeploy_tx_do_create_design_order';
	var CREATE_DEFAULT_ITEM_DESIGN = 'customdeploy_tx_do_create_item_design';
	var CREATE_MATRIX_SUBITEM = 'customdeploy_tx_do_create_matrix_subitem';
	var SET_ITEM_TEAMSHOP = 'customdeploy_tx_do_set_item_teamshop';
	var SET_ITEM_DESIGN_TEAMSHOP = 'customdeploy_tx_do_set_design_teamshop';
	var RESET_ITEM_DESIGN_STATUS = 'customdeploy_tx_do_reset_design_status';
	var REFLECT_ITEM_STATUS_ON_DESIGN = 'customdeploy_tx_do_reflect_item_status';
	var LOCK_DESIGN_ORDER = 'customdeploy_tx_do_lock_design_order';
	var DEFAULT_DESIGN_ORDER_STATUS = 1; //Started
	var DEFAULT_DESIGN_ORDER_ITEM_STATUS = 3; //Pending
    
	/**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	if(context.request.method == 'GET'){
    		switch (runtime.getCurrentScript().deploymentId){
    			case CREATE_DESIGN_ORDER:
    				context.response.write(createDesinOrder(context.request.parameters.offerId));
    				break;
    			case CREATE_DEFAULT_ITEM_DESIGN:
    				context.response.write(createDefaultItemDesign(context.request.parameters.designOrderId));
    				break;
    			case SET_ITEM_TEAMSHOP:
    				context.response.write(setDesignOrderItemTeamshop(context.request.parameters.designOrderId, context.request.parameters.teamshopId));
    				break;
    			case SET_ITEM_DESIGN_TEAMSHOP:
    				context.response.write(setDesignOrderItemDesignTeamshop(context.request.parameters.designOrderId, context.request.parameters.teamshopId));
    				break;
    			case REFLECT_ITEM_STATUS_ON_DESIGN:
    				context.response.write(JSON.stringify(reflectItemStatusOnDesignList(context.request.parameters.designOrderId)));
    				break;
    			case RESET_ITEM_DESIGN_STATUS:
    				context.response.write(JSON.stringify(resetItemDesignStatus(context.request.parameters.designOrderId)));
    				break;
    			case CREATE_MATRIX_SUBITEM:
    				context.response.write(JSON.stringify(createMatrixSubitem(context)));
    				break;
    			case LOCK_DESIGN_ORDER:
    				lockDesignOrder(context);
    				break;
    		}
    	}
    }
    
    var createDesinOrder = function(offerId){
    	var doRec = record.create({ type : DESIGN_ORDER_REC_TYPE_ID});
		var offerRec = record.load({ type : record.Type.ESTIMATE, id : offerId });
		
		doRec.setValue('custrecord_sta_tx_do_custname', offerRec.getValue('entity'));
		doRec.setValue('custrecord_sta_tx_do_salesrep', offerRec.getValue('salesrep'));
		doRec.setValue('custrecord_sta_tx_do_offer', offerRec.id);
		doRec.setValue('custrecord_sta_tx_do_status', DEFAULT_DESIGN_ORDER_STATUS);
		
		for(var i = 0; i < offerRec.getLineCount('item'); i++){
			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_parentitem', i, 
					offerRec.getSublistValue('item', 'item', i));
			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_rate', i,
					offerRec.getSublistValue('item', 'grossamt', i));
			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_pricepar', i,
					offerRec.getSublistValue('item', 'custcol_sta_tx_specialparameter', i));
			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_itemstatus', i, DEFAULT_DESIGN_ORDER_ITEM_STATUS);
			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_createdmanually', i, false);
		}
    	
		return doRec.save(true, true).toString();
    };
    
    var createDefaultItemDesign = function(designOrderId){
    	return designorder.createItemDesign(designOrderId);
    }
    
    var createMatrixSubitem = function(context){
    	var matrixItemsObj = [];
    	var designNumberObj = designorder.getDesignNumbers(context.request.parameters.designOrderId);    	
    	log.debug(designNumberObj);
    	var customlist_sta_tx_sizelistSearchObj = search.create({
			   type: "customlist_sta_tx_sizelist", filters: [],
			   columns: [search.createColumn({name: "name"}),search.createColumn({name: "abbreviation"})]
			});
		
    	var searchResultCount = customlist_sta_tx_sizelistSearchObj.runPaged().count;
    	log.debug("customlist_sta_tx_sizelistSearchObj result count",searchResultCount);

    	
		var customlist_sta_tx_specialsizeSearchObj = search.create({
			   type: "customlist_sta_tx_speciallength", filters: [],
			   columns: [search.createColumn({name: "name"}),search.createColumn({name: "abbreviation"})]
			});
		
		var searchResultCount = customlist_sta_tx_specialsizeSearchObj.runPaged().count;
    	log.debug("customlist_sta_tx_specialsizeSearchObj result count",searchResultCount);
		
		var customrecord_sta_tx_dodetSearchObj = search.load('customsearch_sta_tx_do_item_search');
		customrecord_sta_tx_dodetSearchObj.filterExpression = [["custrecord_sta_tx_itemdesign_parentlink.custrecord_sta_tx_itemdesign_appr","anyof","2"], "AND", 
		                                      			      ["custrecord_sta_tx_dodet_parentlink", "anyof", context.request.parameters.designOrderId],"AND",
		                                     			      ["custrecord_sta_tx_itemdesign_parentlink.custrecord_sta_tx_itemdesign_matrix","is","F"]]
		
		var searchResultCount = customrecord_sta_tx_dodetSearchObj.runPaged().count;
    	log.debug("customrecord_sta_tx_dodetSearchObj result count",searchResultCount);
		
		customrecord_sta_tx_dodetSearchObj.run().each(function(result){
			if(result.getValue({name : 'parent', join: 'CUSTRECORD_STA_TX_DODET_PARENTITEM'})){
				
				var designNumber = designNumberObj[result.getValue({name : 'name', join: 'CUSTRECORD_STA_TX_ITEMDESIGN_PARENTLINK'})] 
				designNumber = (designNumber) ? designNumber : designorder.createDesignNumber(result.getValue({name : 'name', join: 'CUSTRECORD_STA_TX_ITEMDESIGN_PARENTLINK'}),
						result.getValue({name : 'custrecord_sta_tx_itemdesign_name', join: 'CUSTRECORD_STA_TX_ITEMDESIGN_PARENTLINK'}));
				
				log.debug('designNumber', designNumber);
				
				var parentRec = record.load({type : 'assemblyitem', id : result.getValue({name : 'parent', join: 'CUSTRECORD_STA_TX_DODET_PARENTITEM'})});
				
				customlist_sta_tx_specialsizeSearchObj.run().each(function(specialSize){
					
					customlist_sta_tx_sizelistSearchObj.run().each(function(size){
						var itemObj = {};
						itemObj.fields = {};
						
						if (((result.getValue({name: "custrecord_sta_tx_dodet_pricepar"}) == 1 || (!result.getValue({name: "custrecord_sta_tx_dodet_pricepar"}))) && JUNIOR_SIZE.indexOf(size.id) == -1 && specialSize.id == 2)){
							itemObj = renderItemRecord(matrixItemsObj, itemObj, result, size, specialSize, designNumber, parentRec);
							matrixItemsObj.push(itemObj);
						}
						
						if (result.getValue({name: "custrecord_sta_tx_dodet_pricepar"}) == 2 && JUNIOR_SIZE.indexOf(size.id) != -1 && specialSize.id == 2){
							itemObj = renderItemRecord(matrixItemsObj, itemObj, result, size, specialSize, designNumber, parentRec);
							matrixItemsObj.push(itemObj);
						}
						
						if (result.getValue({name: "custrecord_sta_tx_dodet_pricepar"}) == 3 && JUNIOR_SIZE.indexOf(size.id) == -1 && specialSize.id != 2){
							itemObj = renderItemRecord(matrixItemsObj, itemObj, result, size, specialSize, designNumber, parentRec);
							matrixItemsObj.push(itemObj);
						}

						return true;
		    		});
					
					return true;
				});

			}
			return true;
		});
		context.response.write(JSON.stringify(matrixItemsObj));
		var createMatrixTask = task.create({
			taskType : task.TaskType.MAP_REDUCE,
			scriptId : 'customscript_sta_tx_do_create_matrix_mr',
			params : {
				custscript_sta_tx_do_id_mr : context.request.parameters.designOrderId,
				custscript_sta_tx_do_matrix_data_mr : JSON.stringify(matrixItemsObj)
			}
		});
		
		
		var createMatrixTaskStatus = task.checkStatus(createMatrixTask.submit())
		
		var id = record.submitFields({
    		type : DESIGN_ORDER_REC_TYPE_ID,
    		id : context.request.parameters.designOrderId,
    		values : {
    			custrecord_sta_dx_do_matrix_status : createMatrixTaskStatus.status
    		}
    	});
		
		//context.response.write(createMatrixTaskStatus.status)
		
		return matrixItemsObj;
    };
    
    var renderItemRecord = function(matrixItemsObj, itemObj, result, size, specialSize, designNumber, parentRec){
    	itemObj.type = RECORD_TYPE[result.getValue({name: "type", join: "CUSTRECORD_STA_TX_DODET_PARENTITEM"})];
    	
    	//subsidiary
		itemObj.fields.subsidiary = result.getValue({name: "custrecord_sta_tx_do_sub", join: "custrecord_sta_tx_dodet_parentlink"})
		
		//itemid based on item name template
		itemObj.fields.itemid = parentRec.getValue('matrixitemnametemplate').toString();
		itemObj.fields.itemid  = itemObj.fields.itemid.replace('{custitem_sta_tx_design_number}', result.getValue({name : 'name', join: 'CUSTRECORD_STA_TX_ITEMDESIGN_PARENTLINK'}))
		itemObj.fields.itemid  = itemObj.fields.itemid.replace('{custitem_sta_tx_size}', size.getValue({name : 'abbreviation'}));
		itemObj.fields.itemid  = itemObj.fields.itemid.replace('{custitem_sta_tx_speciallength}', specialSize.getValue({name : 'abbreviation'}));
		itemObj.fields.itemid  = itemObj.fields.itemid.replace('{itemid}', parentRec.getValue('itemid'));
				
    	for(var column in result.columns){
    		if(result.columns[column].join && (result.columns[column].join).toUpperCase() == 'CUSTRECORD_STA_TX_DODET_PARENTITEM'.toUpperCase()){
    			itemObj.fields[result.columns[column].name] = result.getValue(result.columns[column]);
    		}
    	}
    	
    	delete itemObj.fields.type;
    	delete itemObj.fields.custitem_sta_tx_size;
    	delete itemObj.fields.custitem_sta_tx_speciallength;
    	delete itemObj.fields.expenseaccount;
    	
    	//replace expenseaccount with cogsaccount
		itemObj.fields.cogsaccount = result.getValue({name: 'expenseaccount', join: 'CUSTRECORD_STA_TX_DODET_PARENTITEM'});

		//matrix options
		itemObj.fields.matrixtype = 'CHILD'
		itemObj.fields.matrixoptioncustitem_sta_tx_size = size.id;
		itemObj.fields.matrixoptioncustitem_sta_tx_speciallength = specialSize.id;
		itemObj.fields.matrixoptioncustitem_sta_tx_design_number = designNumber;
		
	    //demandware categories
	    itemObj.fields.custitem_sta_tx_dwcat = result.getValue({name: "custrecord_sta_tx_dodet_dwcat"});
	    itemObj.fields.custitem_sta_tx_dwcolor = result.getValue({name: "custrecord_sta_tx_itemdesign_color", join: "CUSTRECORD_STA_TX_ITEMDESIGN_PARENTLINK"});
	    
	    //intermediate
	    //itemObj.fields.custitem_sta_tx_intermediate_checkbox = (INTERMEDIATE_MASTER_SIZE.indexOf(size.id) != -1);
	    
	    itemObj.price = {};
		itemObj.price.currency = result.getValue({name: "custrecord_sta_tx_do_curr", join: "custrecord_sta_tx_dodet_parentlink"});
		itemObj.price.rate = result.getValue({name: "custrecord_sta_tx_dodet_rate"});
		
		itemObj.design = {};
		itemObj.design.id = result.getValue({name: "internalid", join: "CUSTRECORD_STA_TX_ITEMDESIGN_PARENTLINK"});
    	
		return itemObj;
    };
    
    var setDesignOrderItemTeamshop = function(designOrderId, teamshopId){
		return designorder.setItemTeamshop(designOrderId, teamshopId);
    };
    
    var setDesignOrderItemDesignTeamshop = function(designOrderId, teamshopId){
    	return designorder.setDesignTeamshop(designOrderId, teamshopId);
    }
    
    var resetItemDesignStatus = function(designOrderId){
    	var doRec = record.load({ type : DESIGN_ORDER_REC_TYPE_ID, id : designOrderId });
		for(var i = 0; i < doRec.getLineCount('recmachcustrecord_sta_tx_dodet_parentlink'); i++){
			if(doRec.getSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_itemstatus', i) == 2)
			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_itemstatus', i, 1)
		}
		
		for(var i = 0; i < doRec.getLineCount('recmachcustrecord_sta_tx_itemdesign_do'); i++){
			doRec.setValue('custrecord_sta_tx_do_status', 5);
			if(doRec.getSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord_sta_tx_itemdesign_appr', i) != 3)
			doRec.setSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord_sta_tx_itemdesign_appr', i, 1)
		}
		
		return doRec.save();
    };
    
    var reflectItemStatusOnDesignList = function(designOrderId){
    	var doRec = record.load({ type : DESIGN_ORDER_REC_TYPE_ID, id : designOrderId, isDynamic : true });
		for (var i = 0; i < doRec.getLineCount('recmachcustrecord_sta_tx_itemdesign_do'); i++){
			doRec.selectLine('recmachcustrecord_sta_tx_itemdesign_do', i);
			var itemIndex = doRec.findSublistLineWithValue('recmachcustrecord_sta_tx_dodet_parentlink', 'id', 
					doRec.getCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_do','custrecord_sta_tx_itemdesign_parentlink'))
			
    		if(itemIndex > -1){
    			doRec.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord1400', 
    					doRec.getSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_itemstatus', itemIndex));
        		doRec.commitLine('recmachcustrecord_sta_tx_itemdesign_do');
    		}
		}
		return doRec.save();
    };
    
    var lockDesignOrder = function(context){
    	var errorPageFile = file.load({ id : './trimtex_design_order_error_page.html' });
		var renderer = render.create();
		
		renderer.templateContent = errorPageFile.getContents();
		renderer.renderToResponse({ response : context.response });
    };
    
    return {
        onRequest: onRequest
    };
    
});
