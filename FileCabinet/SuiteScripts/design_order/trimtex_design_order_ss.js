/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search', './trimtex_design_order_cs_v2.js', 'N/task'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(record, runtime, search, designorder, task) {
	
	var DESIGN_ORDER_REC_TYPE_ID = 'customrecord_sta_tx_do';
	var DEFAULT_DESIGN_ORDER_STATUS = 1; //Started
	var DEFAULT_DESIGN_ORDER_ITEM_STATUS = 3; //Pending
	var DESIGN_ORDER_ITEM_APPROVE_STATUS = 2;//Design Approved
	var ACTION_CREATE_DESIGN_ORDER = 1;
	var ACTION_APPLY_TEAMSHOP = 2;
	var ACTION_CREATE_INTERMEDIATE_ITEM = 3;
	
    /**
     * Definition of the Scheduled script trigger point.
     *
     * @param {Object} scriptContext
     * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
     * @Since 2015.2
     */
    function execute(scriptContext) {
    	
    	var offerId = runtime.getCurrentScript().getParameter({name : 'custscript_sta_tx_do_create_offer'});
    	var designOrderId = runtime.getCurrentScript().getParameter({name : 'custscript_sta_tx_do_id'});
    	var teamshopId = runtime.getCurrentScript().getParameter({name : 'custscript_sta_tx_do_teamshop_id'});
    	var action = runtime.getCurrentScript().getParameter({name : 'custscript_sta_tx_do_action'});
    	//if(!offerId) return false;
    	
    	log.debug('START', '*************');
    	
    	switch(action){
    	case ACTION_CREATE_DESIGN_ORDER:
    		createDesignOrder(offerId);
    		break;
    	case ACTION_APPLY_TEAMSHOP:
    		designorder.setItemTeamshop(designOrderId, teamshopId);
            designorder.setDesignTeamshop(designOrderId, teamshopId);
    		break;
    	case ACTION_CREATE_INTERMEDIATE_ITEM:
    		createIntermediateParent(designOrderId)
    		break;
    	}
    	
		log.debug('END', '*************');
    }
    
    var createDesignOrder = function(offerId){
    	var offerRec = record.load({ type : record.Type.ESTIMATE, id : offerId });
    	var doRec = record.create({ type : DESIGN_ORDER_REC_TYPE_ID});
		
		doRec.setValue('custrecord_sta_tx_do_custname', offerRec.getValue('entity'));
		doRec.setValue('custrecord_sta_tx_do_salesrep', offerRec.getValue('salesrep'));
		doRec.setValue('custrecord_sta_tx_do_offer', offerRec.id);
		doRec.setValue('custrecord_sta_tx_do_status', DEFAULT_DESIGN_ORDER_STATUS);
		
		for(var i = 0; i < offerRec.getLineCount('item'); i++){
			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_parentitem', i, 
					offerRec.getSublistValue('item', 'item', i));
			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_rate', i,
					offerRec.getSublistValue('item', 'grossamt', i));
			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_pricepar', i,
					offerRec.getSublistValue('item', 'custcol_sta_tx_specialparameter', i));
			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_itemstatus', i, DEFAULT_DESIGN_ORDER_ITEM_STATUS);
			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_createdmanually', i, false);
		}
		var doRecId = doRec.save(true, true);
		log.debug('DO Created', doRecId);
		designorder.createItemDesign(doRecId);
    };
    
    var createIntermediateParent = function(designOrderId){
    	if(!designOrderId) return false;
    	
    	//var intermediateItemObj = [];
    	var doRec = record.load({ type : DESIGN_ORDER_REC_TYPE_ID, id : designOrderId });
    	var customrecord_sta_tx_dodetSearchObj = search.create({ type: "customrecord_sta_tx_dodet",
    		   filters:[["custrecord_sta_tx_dodet_itemstatus","anyof",DESIGN_ORDER_ITEM_APPROVE_STATUS], 
    		      "AND", ["custrecord_sta_tx_dodet_parentlink","anyof",designOrderId], "AND", ["formulatext: {custrecord_sta_tx_dodet_intermediate}","isempty",""]],
    		   columns:[
    		      search.createColumn({name: "custrecord_sta_tx_dodet_parentlink"}),
    		      search.createColumn({name: "custrecord_sta_tx_dodet_parentitem"}),
    		      search.createColumn({name: "parent", join: "CUSTRECORD_STA_TX_DODET_PARENTITEM"})]
    		});
    		
    		customrecord_sta_tx_dodetSearchObj.run().each(function(result){
    		   // .run().each has a limit of 4,000 results
    			var intermediateItem  = record.copy({type : record.Type.ASSEMBLY_ITEM, id : result.getValue({name: "parent", join: "CUSTRECORD_STA_TX_DODET_PARENTITEM"})})
    			intermediateItem.setValue('itemid', intermediateItem.getValue('mpn') + '-' + designOrderId);
    			
    			log.debug(intermediateItem.getValue('itemid'));
    			
    			intermediateItem.setValue('custitem_sta_tx_link_grandparent_item', result.getValue({name: "parent", join: "CUSTRECORD_STA_TX_DODET_PARENTITEM"}));
    			intermediateItem.setValue('custitem_sta_tx_intermediate_checkbox', true);
    			intermediateItem.setValue('custitem_sta_tx_design_number', '');
    			intermediateItem.setValue('custitem_sta_tx_speciallength', '');
    			intermediateItem.setValue('custitem_sta_tx_size', '');
    			intermediateItem.setValue('custitem1', '');
    			intermediateItemId = intermediateItem.save();
    			
    			log.debug(intermediateItemId);
    			
    			var index = doRec.findSublistLineWithValue('recmachcustrecord_sta_tx_dodet_parentlink', 'id', result.id);
    			doRec.setSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_intermediate', index, intermediateItemId);
    			//intermediateItemObj.push({id : result.id, itemId : intermediateItemId});
    			
    		   return true;
    		});
    	
    		//doRec.save();
    		
    		var createMatrixTask = task.create({
    			taskType : task.TaskType.MAP_REDUCE,
    			scriptId : 'customscript_sta_tx_do_create_matrix_mr',
    			params : {
    				custscript_sta_tx_do_id_mr : doRec.save()
    				//custscript_sta_tx_do_matrix_data_mr : JSON.stringify(matrixItemsObj)
    			}
    		});
    		
    		
    		var createMatrixTaskStatus = task.checkStatus(createMatrixTask.submit())
    		
    		var id = record.submitFields({
        		type : DESIGN_ORDER_REC_TYPE_ID,
        		id : designOrderId,
        		values : {
        			custrecord_sta_dx_do_matrix_status : createMatrixTaskStatus.status
        		}
        	});
    	
    }
    
    return {
        execute: execute
    };
    
});
