/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/runtime', 'N/search', 'N/task'],
/**
 * @param {record} record
 * @param {runtime} runtime
 * @param {search} search
 */
function(record, runtime, search, task) {
   
	var INTERMEDIATE_MASTER_SIZE_DEFAULT = '18';
	var INTERMEDIATE_MASTER_JUNIOR_SIZE_DEFAULT = '19';
	var doId = runtime.getCurrentScript().getParameter({name : 'custscript_sta_tx_do_id_mr_2'});
    /**
     * Marks the beginning of the Map/Reduce process and generates input data.
     *
     * @typedef {Object} ObjectRef
     * @property {number} id - Internal ID of the record instance
     * @property {string} type - Record type id
     *
     * @return {Array|Object|Search|RecordRef} inputSummary
     * @since 2015.1
     */
    function getInputData() {
    	
    	var intermediateMasterObj = [];
    	var itemsObj = [];
    	
    	var customrecord_sta_tx_dolinkSearchObj = search.load('customsearch_sta_tx_do_link_to_item');
    	
    	customrecord_sta_tx_dolinkSearchObj.filterExpression = [["formulatext: {custrecord_sta_tx_dolink_do_item}","isnotempty",""], "AND", 
    	                                                        ["formulatext: {custrecord_sta_tx_dolink_do_design}","isnotempty",""], "AND", 
    	                                                        ["formulatext: {custrecord_sta_tx_dolink_item.custitem_sta_tx_link_intermediate_item}","isempty",""], "AND",
    	                                                        ["custrecord_sta_tx_dolink_do", "anyof", doId]]
    	
		customrecord_sta_tx_dolinkSearchObj.run().each(function(result){
		   // .run().each has a limit of 4,000 results
			
			if(result.getValue({name: "custrecord_sta_tx_dodet_pricepar",join: "CUSTRECORD_STA_TX_DOLINK_DO_ITEM"}) == '1' && result.getValue({name: "custitem_sta_tx_intermediate_checkbox", join: "CUSTRECORD_STA_TX_DOLINK_ITEM"}) && result.getValue({name: "custitem_sta_tx_size", join: "CUSTRECORD_STA_TX_DOLINK_ITEM"}) == INTERMEDIATE_MASTER_SIZE_DEFAULT){
				intermediateMasterObj.push(result);
			}
			
			if(result.getValue({name: "custrecord_sta_tx_dodet_pricepar",join: "CUSTRECORD_STA_TX_DOLINK_DO_ITEM"}) == '2' && result.getValue({name: "custitem_sta_tx_intermediate_checkbox", join: "CUSTRECORD_STA_TX_DOLINK_ITEM"}) && result.getValue({name: "custitem_sta_tx_size", join: "CUSTRECORD_STA_TX_DOLINK_ITEM"}) == INTERMEDIATE_MASTER_JUNIOR_SIZE_DEFAULT){
				intermediateMasterObj.push(result);
			}
			
		   return true;
		});
		
		var customrecord_sta_tx_dolinkSearchObj = search.load('customsearch_sta_tx_do_link_to_item');
		customrecord_sta_tx_dolinkSearchObj.run().each(function(result){
		   // .run().each has a limit of 4,000 results
			if(!result.getValue({name: "custitem_sta_tx_intermediate_checkbox", join: "CUSTRECORD_STA_TX_DOLINK_ITEM"})){
				var intermediateMaster = intermediateMasterObj.filter(function(obj){
					return obj.getValue('custrecord_sta_tx_dolink_do_design') == result.getValue('custrecord_sta_tx_dolink_do_design');
				});
				
				var item = {};
				item.id = result.getValue('custrecord_sta_tx_dolink_item');
				item.intermediateMaster = (intermediateMaster.length > 0) ? intermediateMaster[0].getValue('custrecord_sta_tx_dolink_item') : '';
				
				itemsObj.push(item);
			}
			
		   return true;
		});
    	log.debug('intermediateMasterObj', intermediateMasterObj);
		log.debug('itemsObj', itemsObj);
		return itemsObj;
    }

    /**
     * Executes when the map entry point is triggered and applies to each key/value pair.
     *
     * @param {MapSummary} context - Data collection containing the key/value pairs to process through the map stage
     * @since 2015.1
     */
    function map(context) {
    	var valueObj = JSON.parse(context.value);
    	
    	log.debug('map ' + valueObj.id, valueObj);
    	
    	context.write(valueObj.id, valueObj);
    }

    /**
     * Executes when the reduce entry point is triggered and applies to each group.
     *
     * @param {ReduceSummary} context - Data collection containing the groups to process through the reduce stage
     * @since 2015.1
     */
    function reduce(context) {
    	for(var i = 0; i < context.values.length; i++){
    		
    		var matrixObj = JSON.parse(context.values[i]);
    		
    		var matrixSubRecId = record.submitFields({
        		type : 'assemblyitem',
        		id : matrixObj.id,
        		values : {
        			custitem_sta_tx_link_intermediate_item : matrixObj.intermediateMaster
        		}
        	});
    		

			context.write(matrixSubRecId, matrixObj);
    		
    	}
    }


    /**
     * Executes when the summarize entry point is triggered and applies to the result set.
     *
     * @param {Summary} summary - Holds statistics regarding the execution of a map/reduce script
     * @since 2015.1
     */
    function summarize(summary) {
    	summary.output.iterator().each(function(key, value){
    		log.debug(key, value);
    	});
    	
    	summary.reduceSummary.errors.iterator().each(function(key, error){
    		log.error('ERROR_CREATING_SUBITEM', key + ' : ' + error);
    	});
    	
    	record.submitFields({
    		type : 'customrecord_sta_tx_do',
    		id : doId,
    		values : {
    			custrecord_sta_dx_do_matrix_status : 'COMPLETE'
    		}
    	});
    }

    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
    
});
