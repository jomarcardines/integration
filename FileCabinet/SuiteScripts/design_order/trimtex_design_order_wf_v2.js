/**
 * @NApiVersion 2.x
 * @NScriptType workflowactionscript
 */
define(['N/runtime'],

function(runtime) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @Since 2016.1
     */
    function onAction(scriptContext) {
    	
    	var newRecord = scriptContext.newRecord;
    	var fromStatus = runtime.getCurrentScript().getParameter('custscript_sta_tx_do_item_from_status');
    	var toStatus = runtime.getCurrentScript().getParameter('custscript_sta_tx_do_item_to_status');
    	
    	if(fromStatus && toStatus){
    		
    		for(var i = 0; i < newRecord.getLineCount('recmachcustrecord_sta_tx_dodet_parentlink'); i++){
    			newRecord.selectLine({sublistId: 'recmachcustrecord_sta_tx_dodet_parentlink', line: i});
    			if(fromStatus === newRecord.getCurrentSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_itemstatus')){
    				
    				newRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_itemstatus', toStatus);
    				newRecord.commitLine('recmachcustrecord_sta_tx_dodet_parentlink');

    			}
    		}
    		
    	}
    	
    	var fromDesignStatus = runtime.getCurrentScript().getParameter('custscript_sta_tx_do_design_from_status');
    	var toDesignStatus = runtime.getCurrentScript().getParameter('custscript_sta_tx_do_design_to_status')
    	
    	log.debug(fromDesignStatus);
    	log.debug(toDesignStatus);
    	
    	if(fromDesignStatus && toDesignStatus){
    		
    		for(var i = 0; i < newRecord.getLineCount('recmachcustrecord_sta_tx_itemdesign_do'); i++){
    			newRecord.selectLine({sublistId: 'recmachcustrecord_sta_tx_itemdesign_do', line: i});
    			if(fromDesignStatus === newRecord.getCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord_sta_tx_itemdesign_appr')){
    				
    				newRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord_sta_tx_itemdesign_appr', toDesignStatus);
    				newRecord.commitLine('recmachcustrecord_sta_tx_itemdesign_do');

    			}
    		}
    		
    	}
    }

    return {
        onAction : onAction
    };
    
});
