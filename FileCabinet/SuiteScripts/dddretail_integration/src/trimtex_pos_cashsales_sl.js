/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['./trimtex_pos_cashsales_lib.js', 'N/https'],

function(cashSale, https) {
   
    /**
     * Definition of the Suitelet script trigger point.
     *
     * @param {Object} context
     * @param {ServerRequest} context.request - Encapsulation of the incoming request
     * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
     * @Since 2015.2
     */
    function onRequest(context) {
    	var ctx = context, logObj = {};
    	var txCsId = ctx.request.parameters.txCsId;
    	
    	if(txCsId){
    		try{
    			var cashSaleId = cashSale.create(txCsId);
        		logObj.type = "SUCCESS";
    			logObj.title = "Record Created";
    			logObj.description = "Cash sale has been crated:" + cashSaleId;
    			cashSale.log(txCsId, logObj);
    			
    			ctx.response.sendRedirect({
        			type: https.RedirectType.RECORD,
        			identifier: 'customrecord_sta_tx_pos_cashsales',
        			id: txCsId
        		});
    			
    		}catch(e){
    			logObj.type = "ERROR";
    			logObj.title = e.name;
    			logObj.description = e;
    				
    			cashSale.log(txCsId, logObj);
    		}
    		
    		
    	}
    }

    return {
        onRequest: onRequest
    };
    
});
